package com.firmanpro.flash;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.apache.commons.lang.StringUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "ser_main", spaceLine = " | ", breakLine = "\n ";
    private static final String type_desc = "DESC", type_asc = "ASC";
    private final int lengthLetter = 10, lengthNumber = 4, maxVolumeTruckTotal = 20;

    private ArrayList<Integer> ar_idbarang_sort_profit = new ArrayList<Integer>();
    private ArrayList<Integer> ar_idbarang_sort_berat = new ArrayList<Integer>();
    private ArrayList<Integer> ar_idbarang_sort_densitas = new ArrayList<Integer>();

    private ArrayList<Integer> ar_posisi_barang = new ArrayList<Integer>();
    private ArrayList<String> ar_nama_barang = new ArrayList<String>();
    private ArrayList<Integer> ar_berat_barang = new ArrayList<Integer>();
    private ArrayList<Integer> ar_profit_barang = new ArrayList<Integer>();
    private ArrayList<Double> ar_hasil_densitas = new ArrayList<Double>();

    private ArrayList<Integer> ar_idposisi_profit = new ArrayList<Integer>();
    private ArrayList<Integer> ar_idposisi_berat = new ArrayList<Integer>();
    private ArrayList<Integer> ar_idposisi_densitas = new ArrayList<Integer>();

    private HashMap<Integer,Integer> hm_data_barang1 = null;
    private HashMap<Integer,Double> hm_data_barang2 = null;

    private DecimalFormat df1 = new DecimalFormat("#.#");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cleanDataBarang();
        enterDataBarang();
        loadDataBarang();
        /**KG=KnapsackGreedy*/
        processKGProfit();
        processKGBerat();
        processKGDensitas();



    }

    private void processKGDensitas() {
        /**
         * DENSITAS
         * #P1. save ke hashMap id_barang & densitas
         * #P2. sort densitas tertinggi dari hashMap
         * #P3. save ke arrayList ar_idbarang_sort_densitas yang sudah diurutkan
         * #P4. munculkan data ar_idposisi_densitas
         * */
        /**#P1*/

        hm_data_barang2 = new HashMap<Integer, Double>();
        hm_data_barang2.clear();
        for (int a = 0; a< ar_posisi_barang.size(); a++) {
            hm_data_barang2.put(ar_posisi_barang.get(a), ar_hasil_densitas.get(a));
        }

        /**#P2*/
        Map<Integer, Integer> map = sortByValues(hm_data_barang2, type_desc);
        Set set2 = map.entrySet();
        Iterator iterator2 = set2.iterator();
        while(iterator2.hasNext()) {
            Map.Entry me2 = (Map.Entry)iterator2.next();
            /**#P3*/
            ar_idbarang_sort_densitas.add(Integer.valueOf(String.valueOf(me2.getKey())));
        }

        /**#P4*/
        Log.d(TAG, breakLine);
        Log.d(TAG, "Greedy By Density");
        int idbrg = 0;
        String statusBrg = "";
        int totalBeratNow = 0;
        for (int b = 0; b< ar_idbarang_sort_densitas.size(); b++){
            statusBrg = "";
            idbrg = ar_idbarang_sort_densitas.get(b);
            if (ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal && totalBeratNow <= maxVolumeTruckTotal){
                if (totalBeratNow+ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal) {
                    statusBrg = spaceLine + strFix("Diambil", lengthLetter);
                    totalBeratNow += ar_berat_barang.get(idbrg);
                    ar_idposisi_densitas.add(ar_posisi_barang.get(idbrg));
                }else {
                    statusBrg = spaceLine;
                }
            }else {
                statusBrg = spaceLine;
            }
            Log.d(TAG,
                    strFix(ar_posisi_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                            strFix(ar_nama_barang.get(idbrg), lengthLetter) + spaceLine +
                            strFix(ar_berat_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                            strFix(ar_profit_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                            strFix(ar_hasil_densitas.get(idbrg).toString(), lengthNumber) + statusBrg

            );
        }

    }

    private void processKGBerat() {
        /**
         * BERAT
         * #P1. save ke hashMap id_barang & berat_barang
         * #P2. sort berat_barang teringan dari hashMap
         * #P3. save ke arrayList ar_idbarang_sort_berat yang sudah diurutkan
         * #P4. munculkan data ar_idposisi_berat
         * */

        /**#P1*/
        hm_data_barang1 = new HashMap<Integer, Integer>();
        hm_data_barang1.clear();
        for (int a = 0; a< ar_posisi_barang.size(); a++) {
            hm_data_barang1.put(ar_posisi_barang.get(a), ar_berat_barang.get(a));
        }

        /**#P2*/
        Map<Integer, Integer> map = sortByValues(hm_data_barang1, type_asc);
        Set set2 = map.entrySet();
        Iterator iterator2 = set2.iterator();
        while(iterator2.hasNext()) {
            Map.Entry me2 = (Map.Entry)iterator2.next();
            /**#P3*/
            ar_idbarang_sort_berat.add(Integer.valueOf(String.valueOf(me2.getKey())));
        }

        /**#P4*/
        Log.d(TAG, breakLine);
        Log.d(TAG, "Greedy By Weight");
        int idbrg = 0;
        String statusBrg = "";
        int totalBeratNow = 0;
        for (int b = 0; b< ar_idbarang_sort_berat.size(); b++){
            statusBrg = "";
            idbrg = ar_idbarang_sort_berat.get(b);
            if (ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal && totalBeratNow <= maxVolumeTruckTotal){
                if (totalBeratNow+ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal) {
                    statusBrg = spaceLine + strFix("Diambil", lengthLetter);
                    totalBeratNow += ar_berat_barang.get(idbrg);
                    ar_idposisi_berat.add(ar_posisi_barang.get(idbrg));
                }else {
                    statusBrg = spaceLine;
                }
            }else {
                statusBrg = spaceLine;
            }
            Log.d(TAG,
                    strFix(ar_posisi_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                            strFix(ar_nama_barang.get(idbrg), lengthLetter) + spaceLine +
                            strFix(ar_berat_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                            strFix(ar_profit_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                            strFix(ar_hasil_densitas.get(idbrg).toString(), lengthNumber) + statusBrg

            );
        }
    }

    private void processKGProfit() {
        /**
         * PROFIT
         * #P1. save ke hashMap id_barang & harga_barang
         * #P2. sort harga_barang tertinggi dari hashMap
         * #P3. save ke arrayList ar_idbarang_sort_profit yang sudah diurutkan
         * #P4. munculkan data ar_idposisi_profit
         * */

        /**#P1*/
        hm_data_barang1 = new HashMap<Integer, Integer>();
        hm_data_barang1.clear();
        for (int a = 0; a< ar_posisi_barang.size(); a++) {
            Double hsl_dnt = Double.valueOf(ar_profit_barang.get(a)) / Double.valueOf(ar_berat_barang.get(a));
            ar_hasil_densitas.add(Double.valueOf(df1.format(hsl_dnt)));
            hm_data_barang1.put(ar_posisi_barang.get(a), ar_profit_barang.get(a));
        }

        /**#P2*/
        Map<Integer, Integer> map = sortByValues(hm_data_barang1, type_desc);
        Set set2 = map.entrySet();
        Iterator iterator2 = set2.iterator();
        while(iterator2.hasNext()) {
            Map.Entry me2 = (Map.Entry)iterator2.next();
            //Log.d(TAG, me2.getKey() + ": "+me2.getValue());
            /**#P3*/
            ar_idbarang_sort_profit.add(Integer.valueOf(String.valueOf(me2.getKey())));
        }

        /**#P4*/
        Log.d(TAG, breakLine);
        Log.d(TAG, "Greedy By Profit");
        int idbrg = 0;
        String statusBrg = "";
        int totalBeratNow = 0;
        for (int b = 0; b< ar_idbarang_sort_profit.size(); b++){
            statusBrg = "";
            idbrg = ar_idbarang_sort_profit.get(b);
            if (ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal && totalBeratNow <= maxVolumeTruckTotal){
                if (totalBeratNow+ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal) {
                    statusBrg = spaceLine + strFix("Diambil", lengthLetter);
                    totalBeratNow += ar_berat_barang.get(idbrg);
                    ar_idposisi_profit.add(ar_posisi_barang.get(idbrg));
                }else {
                    statusBrg = spaceLine;
                }
            }else {
                statusBrg = spaceLine;
            }


            Log.d(TAG,
                strFix(ar_posisi_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                strFix(ar_nama_barang.get(idbrg), lengthLetter) + spaceLine +
                strFix(ar_berat_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                strFix(ar_profit_barang.get(idbrg).toString(), lengthNumber) + spaceLine +
                strFix(ar_hasil_densitas.get(idbrg).toString(), lengthNumber) + statusBrg
            );
        }
    }

    private void loadDataBarang() {
        Log.d(TAG, "Data Awal");
        for (int a = 0; a< ar_posisi_barang.size(); a++){
            Log.d(TAG,
                strFix(ar_posisi_barang.get(a).toString(), lengthNumber) + spaceLine +
                strFix(ar_nama_barang.get(a), lengthLetter) + spaceLine +
                strFix(ar_berat_barang.get(a).toString(), lengthNumber) + spaceLine +
                strFix(ar_profit_barang.get(a).toString(), lengthNumber) + spaceLine
            );
        }

    }

    private String strFix(String val, int length){
        if (val.length()!=length){
            return StringUtils.repeat(" ", length - val.length()) + val;
        }
        return val;
    }

    private void enterDataBarang() {
        saveDataBarang(0, "Lenovo", 8, 100);
        saveDataBarang(1, "Acer", 4, 50);
        saveDataBarang(2, "Mac", 13, 130);
        saveDataBarang(3, "Samsung", 6, 80);
    }

    private void saveDataBarang(int posisi, String nama, int berat, int profit) {
        ar_posisi_barang.add(posisi);
        ar_nama_barang.add(nama);
        ar_berat_barang.add(berat);
        ar_profit_barang.add(profit);
    }

    private void cleanDataBarang() {
        if (hm_data_barang1 !=null) {
            hm_data_barang1.clear();
        }
        ar_posisi_barang.clear();
        ar_nama_barang.clear();
        ar_berat_barang.clear();
        ar_profit_barang.clear();
        ar_idposisi_profit.clear();
        ar_idbarang_sort_profit.clear();
    }

    private static HashMap sortByValues(HashMap map, final String type) {
        List list = new LinkedList(map.entrySet());
        /**Defined Custom Comparator here*/
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                if (type.equals(type_desc)) {
                    return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
                }else {
                    return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
                }
            }
        });
        /**Here I am copying the sorted list in HashMap
         * using LinkedHashMap to preserve the insertion order*/
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}
